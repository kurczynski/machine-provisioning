# machine-provisioning

Ansible configurations to provision all of the machines I use because I loath
having to manually reconfigure them.

## Create an account to run playbooks

```
useradd --create-home ansible --groups wheel
```

Then export the SSH public key of the user that will run the playbooks to the newly created `ansible` user.

## Secrets

Secrets are (starting to be) stored using Bitwarden and require the Bitwarden
SDK to be installed:

```
pip install bitwarden-sdk
```

And the Ansible `bitwarden.secrets` collection:

```
ansible-galaxy collection install bitwarden.secrets
```

Set the environment variable for the Bitwarden secrets authentication:

```
# Note the space before setting the token, we don't want to put the token in our shell history
 export BWS_ACCESS_TOKEN=<ACCESS_TOKEN_VALUE>
```

## Run:
```
ansible-playbook site.yaml --inventory inventory.yaml --extra-vars @secrets.yaml --vault-password-file some-password-file --limit some-hostname --tags some-tag
```
or:
```
ansible-playbook --ask-become-pass --inventory inventory.yaml site.yaml
```

## Edit vault files:
```
ansible-vault edit --vault-password-file some-password-file some-secrets.yaml
```

**Ansible Vault should not be used** use Bitwarden secrets instead.
